<?php


namespace Engeni\ApiClient\Resource\Yoda;

/*
 * Copyright 2020 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Abstracts\ResourceAbstract;

class Batch extends ResourceAbstract
{
    protected $resourceName = 'yoda/api/v1/batches';

    /**
     * Gets the location.
     *
     * @param $locationId
     * @param array $batch_ids
     * @return array|null
     */
    public static function getBatches($locationId, array $batch_ids): ?array
    {
        $instance = new static();

        $newClient = $instance->getClient();

        $newClient->addHeader('Location', $locationId);

        $path = $instance->resourceName."/multiple/".implode(",",$batch_ids);

        $query = $instance->newQuery()
            ->setClient($newClient)
            ->setPath($path);

        return $query->get();
    }
}