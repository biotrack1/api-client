<?php

namespace Engeni\ApiClient\Resource\Core\Account;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
use Engeni\ApiClient\Resource\Core\BaseResource;

class CreditCard extends BaseResource
{
    protected $resourceName = 'credit-cards';

    protected $parentResource = 'Engeni\ApiClient\Resource\Core\Account\Account';

    public static function star($id, $account_id)
    {
        return self::do('star', 'POST', $id, $account_id);
    }
}
