<?php

namespace Engeni\ApiClient\Resource\Core\Account;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
use Engeni\ApiClient\Resource\Core\BaseResource;
use Engeni\ApiClient\Resource\Core\Country;
use Engeni\ApiClient\Resource\Core\Location;
use Engeni\ApiClient\Resource\Core\Partner;
use Engeni\ApiClient\Resource\Core\State;
use Engeni\ApiClient\Resource\Core\User;

class Account extends BaseResource implements AccountInterface
{
    protected $resourceName = 'accounts';

    protected $relations = [
        'locations' => Location::class,
        'partner' => Partner::class,
        'country' => Country::class,
        'state' => State::class,
        'owner' => User::class,
    ];

    public function linkUsers(array $userIds)
    {
        if (!$this->id) {
            throw new Exception("Please load an account to be able to link users.", 1);
        }
        return self::do(
            'users/link',
            [
                'verb' => 'POST',
                'form_params' => $userIds,
            ],
            $this->id
        );
    }

    public function unlinkUsers(array $userIds)
    {
        if (!$this->id) {
            throw new Exception("Please load an account to be able to unlink users.", 1);
        }
        return self::do(
            'users/unlink',
            [
                'verb' => 'POST',
                'form_params' => $userIds,
            ],
            $this->id
        );
    }
}
