<?php

namespace Engeni\ApiClient\Resource\Core\Oauth;

interface AppClientInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string|null
     */
    public function getClientId(): ?string;
}
