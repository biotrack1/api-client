<?php

namespace Engeni\ApiClient\Resource\Core\Permission;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Exceptions\AuthorizationException;
use Engeni\ApiClient\Resource\Core\BaseResource;

class PermissionService extends BaseResource
{
    protected $resourceName = '';

    /**
     * Gets the authorization for a given user/service/resource/action.
     *
     * @param  integer  $user_id
     * @param  string  $service
     * @param  string  $resource
     * @param  string  $action
     * @return boolean
     */

    public static function authorize($user_id, $service, $resource, $action, $pin_code)
    {
        try {
            $instance = new static();
            $client = $instance->getClient();
            $query = $instance->newQuery()
                ->setClient($client)
                ->setPath($instance->rootPath . '/permission/authorize');

            $result = $client->post($query, [
                'user_id' => $user_id,
                'service' => $service,
                'resource' => $resource,
                'action' => $action,
                'pin_code' => $pin_code,
            ]);

            return $result->getStatusCode() == 200;
        } catch (\Throwable $e) {
            throw new AuthorizationException($e, $e->getCode());
        }
    }

    /**
     * Setup static method setups an account's roles.
     * @param  int      $account_id  The account to wich roles must be created
     * @param  int|null $location_id The location wich roles must be applied to (optional);
     * @param  int|null $user_id     The user that has to have the Account Owner Role applied (optional);
     * @return boolean
     */
    public static function setup(int $account_id, int $location_id = null, int $user_id = null)
    {
        return self::do('permission/setup', [
            'verb' => 'POST',
            'form_params' => array_filter([
                'account_id' => $account_id,
                'location_id' => $location_id,
                'user_id' => $user_id,
            ]),
        ]);
    }

    /**
     * Gets the user data from given pin code owner
     *
     * @param $pin_code
     * @return mixed
     */
    public static function getUserByPincode($pin_code)
    {
        try {
            $instance = new static();
            $client = $instance->getClient();
            $query = $instance->newQuery()
                ->setClient($client)
                ->setPath($instance->rootPath . '/permission/user_by_pincode');
            $result = $client->post($query, ['pin_code' => $pin_code]);
            $user = json_decode($result->getBody()->getContents())->data;
            return $user;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
