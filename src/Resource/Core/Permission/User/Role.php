<?php

namespace Engeni\ApiClient\Resource\Core\Permission\User;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Resource\Core\BaseResource;

class Role extends BaseResource
{
    protected $resourceName = 'roles';
    protected $parentResource = 'Engeni\ApiClient\Resource\Core\Permission\User\User';

    /**
     * Syncs roles for a given user
     *
     * @param array $settings
     * @param Int $id
     * @return bool
     */
    public static function sync(array $roles, $user_id)
    {
        if (!$user_id) {
            throw new Exception("Please define a User ID to sync Roles", 1);
        }
        return self::do('sync',
            [
                'verb' => 'PUT',
                'form_params' => $roles,
            ],
            null,
            $user_id
        );
    }

}
