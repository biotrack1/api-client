<?php

namespace Engeni\ApiClient\Resource\Core\Auth;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Resource\Core\BaseResource;

class Password extends BaseResource
{
    protected $resourceName = 'auth/password';

    /**
     * sendLink() sends the reset passwoord link by email.
     * @param  String  $email
     * @return bool
     */
    public static function sendResetLink($email)
    {
        return self::do(
            'email',
            [
                'verb' => 'POST',
                'form_params' => ['email' => $email],
            ]
        );
    }

    /**
     * reset() ends the reset password process
     * @param  String  $token
     * @param  String  $email
     * @param  String  $password
     * @param  String  $password_confirmation
     * @return bool
     */
    public static function reset($token, $email, $password, $password_confirmation)
    {
        return self::do(
            'reset',
            [
                'verb' => 'POST',
                'form_params' => [
                    'token' => $token,
                    'email' => $email,
                    'password' => $password,
                    'password_confirmation' => $password_confirmation,
                ],
            ]
        );
    }
}
