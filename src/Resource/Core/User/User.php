<?php

namespace Engeni\ApiClient\Resource\Core\User;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
use Engeni\ApiClient\Resource\Core\BaseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class User extends BaseResource implements UserInterface
{
    protected $resourceName = 'users';

    /**
     * Gets the user for a given token.
     *
     * @param string $token
     * @param array $embed
     * @return User|null $user
     */
    public static function getByToken($token, $embed = []): ?User
    {
        $instance = new static();

        $newClient = $instance->getClient();

        $headers = collect(app(Request::class)->headers->all())->only('x-user-id', 'x-location-id',
            'x-account-id')->map(function ($item, $key) {
            return head($item);
        });

        $headers->each(function ($value, $key) use ($newClient) {
            $newClient->addHeader(Str::title($key), $value);
        });

        $newClient->addHeader('Authorization', $token);

        $query = $instance->newQuery()
            ->setClient($newClient)
            ->setPath($instance->resourceName . '/get_by_token');

        foreach ($embed as $with) {
            $query->with($with);
        }

        $result = $query->get();

        return $result
        ? $instance->fill(json_decode(json_encode((array) $result[0]), true))
        : null;
    }

    /**
     * Saves settings for a given user.
     *
     * @param array $settings
     * @param Int $id
     * @return bool
     */
    public function saveSettings(array $settings, Int $id = null): bool
    {
        $id = $id ?? $this->id;

        if (!$id) {
            throw new Exception("Please define a User ID to save settings", 1);
        }
        return self::do('settings',
            [
                'verb' => 'PUT',
                'form_params' => $settings,
            ],
            $id
        );
    }

    /**
     * Gets the user timezone
     * @return String
     */
    public function getTimezone(): String
    {
        return 'UTC';
    }

    /**
     * Gets a specific user setting or null
     * @param String $attribute
     * @return mixed
     */
    public function getSetting(String $attribute)
    {
        if (isset($this->settings[$attribute])) {
            return $this->settings[$attribute];
        }

        return null;
    }

    /**
     * @param string $imagePath
     * @return User
     */
    public function setAvatar(string $imagePath): User
    {
        $this->getClient()->addFile('avatar', $imagePath);

        return $this;
    }
}
