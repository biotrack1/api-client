<?php

namespace Engeni\ApiClient\Resource\Core\User;

interface UserInterface {
    
    public function getTimezone();
        
}
