<?php

namespace Engeni\ApiClient;

class ClientBuilder
{
    public function setCLIClient(int $userId = null, int $locationId = null, int $accountId = null, $type = null)
    {
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $client_id = config('engeni.oauth_client_id');
        $client_secret = config('engeni.oauth_client_secret');
        $oauth_version = config('engeni.oauth_version', null); // oauth 2 is the default supported version

        // For Client credentials please read "Configuring a User Pool App Client > Allowed OAuth Flows > Client credentials",
        // here: https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-app-idp-settings.html
        // To understand how to configure API Gateway + Cognito, please watch this video (all of it): https://www.youtube.com/watch?v=4n5Ssr3NZRc
        $oauth_scopes = config('engeni.oauth_scopes', null);

        if ($oauth_version == 1) {
            $response = $http->request('POST', config('engeni.api-client.base_uri') . '/oauth/token', [
                'headers' => [
                    'cache-control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'grant_type' => 'client_credentials',
                    'scopes' => $oauth_scopes,
                ],
            ]);
        } else if (!$oauth_version || $oauth_version == 2) {
            $response = $http->request('POST', config('engeni.oauth_api_url') . '/oauth2/token', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                ],
                'auth' => [$client_id, $client_secret],
            ]);
        } else {
            throw \Exception('OAuth version not supported');
        }

        $token = json_decode((string) $response->getBody(), true)['access_token'];

        Client::forgetInstance();

        if(!is_null($type) && $type === 'SCRIPT_UPLOAD') {
            $headers = array_filter(['X-User-ID' => $userId, 'X-Account-ID' => $accountId]);
        } else {
            $headers = array_filter(['X-User-ID' => $userId, 'X-Account-ID' => $accountId, 'X-Location-ID' => $locationId]);
        }

        $client = new Client([
            'user_class' => config('engeni.api-client.user_class', false),
            'base_uri' => config('engeni.api-client.base_uri'),
            'debug' => config('engeni.api-client.debug', false),
            'timeout' => config('engeni.api-client.timeout', 30),
            'auth_token' => $token,
            'headers' => $headers,
        ]);

        $client->setAsGlobal();
    }
}
