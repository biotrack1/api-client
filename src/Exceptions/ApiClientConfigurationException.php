<?php

namespace Engeni\ApiClient\Exceptions;

class ApiClientConfigurationException extends ApiClientException
{
    public function __construct($message = null, $code = 500)
    {
        parent::__construct($message, $code);
    }
}