<?php

namespace Engeni\ApiClient\Exceptions;

use GuzzleHttp\Exception\ClientException;

/**
 * Standard exception thrown by Engeni Api Client
 */
class AuthorizationException extends \Exception
{
    const ERROR_CODE = 403;
    const ERROR_MESSAGE = 'Forbbiden';

    /**
     * Class constructor
     *
     * @param ClientException|null $error Error message
     * @param int|null $code Error code
     */
    public function __construct(ClientException $error = null, int $code = null)
    {
        parent::__construct(
            $this->getErrorMessage($error) ?? self::ERROR_MESSAGE,
            $code ?: self::ERROR_CODE
        );
    }

    private function getErrorMessage(ClientException $error = null):? string
    {
        if ($error !== null) {
            $bodyError = $error->getResponse()->getBody();
            $processedResponse = json_decode($bodyError, true);

            return data_get($processedResponse, 'message', data_get($processedResponse, 'error.message'));
        }
        return null;
    }
}
