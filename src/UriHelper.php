<?php

namespace Engeni\ApiClient;

use Engeni\ApiClient\Exceptions\ApiClientConfigurationException;

class UriHelper
{
    const VALID_PREFIXES = ['yoda', 'core', 'bdi', 'bcko', 'acct'];

    private static function getProxyServices(): array
    {
        $proxies = json_decode(config('engeni.api-client.proxies'), true);
        if ($proxies) {
            return $proxies;
        }

        throw new ApiClientConfigurationException('ENGENI_APICLIENT_PROXIES environment configuration is missing or has an unrecognized format');
    }

    private static function getProxyServicesIndexedByPrefix(): array
    {
        $proxiesByPrefix = [];
        $proxies = self::getProxyServices();

        foreach ($proxies as $proxy) {
            $proxiesByPrefix[$proxy['prefix']] = $proxy;
        }

        return $proxiesByPrefix;
    }

    public static function getProxyServicesByPrefix(string $prefix): array
    {
        if (config('engeni.api-client.use_api_gateway', false)) {
            $api_gateway_url = config('engeni.api-client.base_uri');
            if (!$api_gateway_url) {
                throw new ApiClientConfigurationException('ENGENI_APICLIENT_URL environment configuration is missing');
            }
            if (!in_array($prefix, self::VALID_PREFIXES)) {
                $service = 'core';
            } else {
                $service = $prefix;
            }
            return ['prefix' => $service, 'baseUri' => $api_gateway_url];
        }

        $proxyServicesIndexedByPrefix = self::getProxyServicesIndexedByPrefix();

        return $proxyServicesIndexedByPrefix[$prefix] ?? [];
    }

    public static function getPathWithoutPrefix(string $path): string
    {
        $parts = explode('/', $path);
        if (in_array($parts[0], self::VALID_PREFIXES)) {
            $result = implode('/', array_slice($parts, 1));
        } else {
            $result = $path;
        }

        return $result;
    }
}
