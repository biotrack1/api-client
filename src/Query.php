<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Exceptions\ResourceNotFoundException;

class Query
{
    /**
     * The path where the query is executed
     * @var string
     */
    private $path = null;

    /**
     * The path where the query is executed
     * @var string
     */
    private $resource = null;

    /**
     * @var []
     */
    private $with = [];

    /**
     * @var []
     */
    private $filters = [];

    /**
     * @var []
     */
    private $sorting = [];

    /**
     * @var []
     */
    private $columns = [];

    /**
     * @var bool
     */
    private $pagination = false;

    /**
     * @var bool
     */
    private $limit = null;

    /**
     * @var string
     */
    private $whereKeyName;

    /**
     * @var array
     */
    private $forPage = [];

    /**
     * @var Engeni\ApiClient\Client
     */
    private $client;

    /**
     * Sets the client to execute the query
     *
     * @param  \Engeni\ApiClient\Client $client
     * @return \Engeni\ApiClient\Query
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Creates new resource within a diffent App Context.
     * @param  array  $context ['account_id',  'location_id']
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function withinContext(array $appContext)
    {
        if (isset($appContext['account_id'])) {
            $this->client->addHeader(Client::APP_CONTEXT_ACCOUNT_HEADER, $appContext['account_id']);
        }

        if (isset($appContext['location_id'])) {
            $this->client->addHeader(Client::APP_CONTEXT_LOCATION_HEADER, $appContext['location_id']);
        }

        return $this;
    }

    /**
     * Sets the path to execute the query
     *
     * @param  string  $path
     * @return Engeni\ApiClient\Query
     */
    public function setPath($path = '/')
    {
        if (strpos($path, 'https://') !== false) {
            $this->path = $path;
        } else {
            $parts = explode('/', $path);
            $service = UriHelper::getProxyServicesByPrefix($parts[0]);

            if ($service == []) {
                $service = UriHelper::getProxyServicesByPrefix('core');
            }

            $this->path = rtrim($service['baseUri'], "/") . '/' . UriHelper::getPathWithoutPrefix($path);
        }

        return $this;
    }

    /**
     * Sets the path to execute the query
     *
     * @param  string  $resource
     * @return Engeni\ApiClient\Query
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }

    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Sets the path to execute the query
     *
     * @return Engeni\ApiClient\Query
     */
    public function getPath()
    {
        return $this->path ?? $this->resource->getPath();
    }

    /**
     * Resource key name (i.e. id)
     *
     * @param  string  $keyName
     * @return Engeni\ApiClient\Query
     */
    public function whereKeyName($keyName)
    {
        $this->whereKeyName = $keyName;
        return $this;
    }

    /**
     * @param  string  $columns
     * @return Engeni\ApiClient\Query
     */
    public function select(...$columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * @param  string  $field
     * @param  mixed  $value
     * @return Engeni\ApiClient\Query
     */
    public function where($field, $value)
    {
        if ((string) $field == $this->whereKeyName && !is_array($value)) {
            $this->resource->setKey($value);
        } else {
            $this->filters[$field] = $value;
        }
        return $this;
    }

    /**
     * @param  string  $field
     * @param  array  $values
     * @return Engeni\ApiClient\Query
     */
    public function whereIn($field, array $values = [])
    {
        if (is_array($values)) {
            foreach ($values as $value) {
                $this->filters[$field][] = $value;
            }
        }
        return $this;
    }

    /**
     * @param  int  $limit
     * @return Engeni\ApiClient\Query
     */
    public function limit($limit = 20)
    {
        $this->limit = $limit;
        return $this;
    }

    private function getFilters()
    {
        return $this->filters;
    }

    /**
     * Resource relations (i.e. 'language' where language is a relation of landing landing->language)
     *
     * @param  string  $relations
     * @return Engeni\ApiClient\Query
     */
    public function with($relations)
    {
        $this->with[] = $relations;
        return $this;
    }

    /**
     * @param  string  $field
     * @param  string  $order
     * @return Engeni\ApiClient\Query
     */
    public function orderBy($field, $order = 'ASC')
    {
        $this->sorting[$field] = $order;
        return $this;
    }

    /**
     * @return  string  Prepared string to send to http client
     */
    private function prepareColumns()
    {
        return implode(',', $this->columns);
    }

    /**
     * @return  string  Prepared string to send to http client
     */
    private function prepareWhith()
    {
        return implode(',', $this->with);
    }

    /**
     * @return  string  Prepared string to send to http client
     */
    private function prepareSorting()
    {
        $preparedSorting = [];
        foreach ($this->sorting as $field => $order) {
            $preparedSorting[] = (strtoupper($order) == 'ASC' ? '' : '-') . $field;
        }
        return implode(',', $preparedSorting);
    }

    /**
     * @return  string  Prepared query to send to the http client
     */
    public function getPreparedQuery()
    {
        return array_filter(array_merge(
            $this->forPage,
            $this->getFilters(),
            [
                'fields' => $this->prepareColumns(),
                'embed' => $this->prepareWhith(),
                'sort' => $this->prepareSorting(),
                'limit' => $this->limit ?? null,
                'no_pagination' => ($this->pagination != true) ? 1 : 0,
            ]
        ));
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @return array|null
     */
    public function getFromApi()
    {
        $response = $this->client->get($this);

        // print_r($response);
        if ($response->getStatusCode() == '200') {
            return json_decode($response->getBody());
        } else if ($response->getStatusCode() == '404') {
            return null;
        }
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @return array|null
     */
    public function get()
    {
        $this->pagination = false;
        $body = $this->getFromApi();

        if (!is_null($body)) {
            return $this->getCollection($body->data);
        }
        return null;
    }

    /**
     * Alias from get()
     *
     * @return array|null
     */
    public function all()
    {
        return $this->get();
    }

    /**
     * Set the limit and offset for a given page.
     * page and per_page are strings that cannot be changed as they belong to the Engeni API definition.
     *
     * @param  int  $page
     * @param  int  $perPage
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function forPage($page, $perPage = 15)
    {
        $this->forPage = [
            'page' => $page,
            'per_page' => $perPage,
        ];
    }

    /**
     * Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return Engeni\ApiClient\Paginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $columns = [], $pageName = 'page', $page = null)
    {
        $this->pagination = true;

        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $perPage = (int) $perPage ?: $this->resource->getPerPage();

        $this->columns = $columns ?: $this->columns;

        $this->forPage($page, $perPage);

        $body = $this->getFromApi();

        if ($body != null) {
            return new Paginator((array) $body->data);
        }
        return null;
    }

    private function getCollection($data)
    {
        $collection = [];
        if (is_array($data)) {
            foreach ($data as $resource) {
                // $collection[] = new $resourceClassName((array) $resource);
                $collection[] = $resource;
            }
        } else {
            $collection[] = $data;
        }
        return $collection;
    }

    /**
     * Execute the query as a "first" statement.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract|null
     */
    public function first()
    {
        $collection = $this->limit(1)->get();
        if (isset($collection[0])) {
            $this->resource->fill((array) $collection[0]);
            return $this->resource;
        }
        return null;
    }

    /**
     * Find a resource by its primary key (and parent Key | optional).
     *
     * @param  mixed $id
     * @param  array $columns
     * @return \Engeni\ApiClient\Resource\ResourceAbastract|null
     */
    public function find($id)
    {
        return $this->where($this->whereKeyName, $id)->first();
    }

    /**
     * Find a resource by its primary key or throw an exception.
     *
     * @param  mixed $id
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     *
     * @throws \ErrorException
     */
    public function findOrFail($id)
    {
        try {
            $result = $this->find($id);
            if (!is_null($result)) {
                return $result;
            }
        } catch (\Exception $e) {
            throw (new ResourceNotFoundException)->setResource($this->resource, $id);
        }
    }

    /**
     * Find a resource by its primary key or create a new one.
     *
     * @param  mixed $id | $parent_id, $id
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     *
     * @throws \ErrorException
     */
    public function findOrNew(...$parameters)
    {
        try {
            $result = $this->find(...$parameters);
            if (!is_null($result)) {
                return $result;
            }
        }
        // Not found. We don't do anything as we need to continue to execute the following code
         catch (\Exception $e) {}

        // If not found, create new instance...
        return $this->resource->setKey($parameters);
    }

    /**
     * Get an array with the values of a given column.
     *
     * @param  string  $column
     * @param  string|null  $key
     * @return []
     */
    public function pluck($column, $key = null)
    {
        if (($collection = $this->get()) != null) {
            $results = [];

            foreach ($collection as $item) {

                // If the key is "null", we will just append the value to the array and keep
                // looping. Otherwise we will key the array using the value of the key we
                // received from the developer. Then we'll return the final array form.
                if (is_null($key)) {
                    $results[] = $item->$column;
                } else {
                    $results[$item->$key] = $item->$column;
                }
            }

            return $results;
        }

        return null;
    }

    /**
     * Create a new Resource recource instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct()
    {
        // app is a Laravel/lumen function
        if (function_exists('app')) {
            $this->client = app(Client::class);
        } else {
            $this->client = Client::class;
        }
    }

    /**
     * Convert the resource to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return http_build_query($this->getPreparedQuery());
    }

    /**
     * Handle dynamic method calls into the method.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        $className = static::class;

        // throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
    }
}
