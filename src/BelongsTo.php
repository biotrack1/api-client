<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class BelongsTo extends Relation
{
    public $query;
    public $child;
    public $foreignKey;
    public $ownerKey;
    public $relation;

    public function __construct($query, $child, $foreignKey, $ownerKey, $relation)
    {
        $this->query = $query;
        $this->model = $child;
        $this->foreignKey = $foreignKey;
        $this->ownerKey = $ownerKey;
        $this->relation = $relation;
    }

    /**
     * Resource relations (i.e. 'language' where language is a relation of landing landing->language)
     *
     * @param  string  $relations
     * @return Engeni\ApiClient\Query
     */
    public function with($relations)
    {
        $this->query->with($relations);
        return $this;
    }
}
