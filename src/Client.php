<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Exceptions\ApiResponseException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class Client
{
    const VERSION = "5.x";
    const USER_AGENT_SUFFIX = "engeni-api-client/";
    const API_BASE_URI = 'https://api.engeni.com';
    const API_BASE_PATH = '/';
    const APP_CONTEXT_ACCOUNT_HEADER = 'X-Account-Id';
    const APP_CONTEXT_LOCATION_HEADER = 'X-Location-Id';

    /**
     * @var GuzzleHttp\ClientInterface $http
     */
    private $http;

    /**
     * @var array access token
     */
    private $token;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var array $headers
     */
    private $headers = [];

    /**
     * @var array $files
     */
    protected $files = [];

    /**
     * @var bool $isMultipart
     */
    protected $isMultipart = false;

    /**
     * Scopes requested by the client
     *
     * @var array $scopes
     */
    protected $requestedScopes = [];

    /**
     * The current globally used instance.
     *
     * @var object
     */
    protected static $instance;

    /**
     * Construct the Engeni Client.
     * See http://docs.guzzlephp.org/en/stable/request-options.html for config $options.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!self::$instance) {
            $this->config = array_merge(
                [
                    'application_name' => '',
                    // Don't change these unless you're working against a special development or testing environment.
                    'base_uri' => self::API_BASE_URI,
                    'base_path' => self::API_BASE_PATH,
                    'user_agent_suffix' => self::USER_AGENT_SUFFIX,
                    'client_id' => '',
                    'client_secret' => '',
                    'state' => null,
                    'auth_token' => null,
                    'timeout' => 10,
                    'retry' => array(),
                    'debug' => false,
                    'verify' => false,
                    // Explicitly pass this in to avoid setting JWT::$leeway
                    'jwt' => null,
                    'headers' => [],
                ],
                $config
            );
        } else {
            $this->config = self::$instance->getConfig();
        }
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Make this capsule instance available globally.
     *
     * @return Client
     */
    public function setAsGlobal(): Client
    {
        static::$instance = $this;

        return $this;
    }

    /**
     * Unregister singleton.
     *
     * @return void
     */
    public static function forgetInstance()
    {
        static::$instance = null;
    }

    /**
     * Add a Header to the Request. Warning, it will overwrite previous headers.
     *
     * @param array $config
     */
    public function addHeader(String $header, String $value): Client
    {
        $this->headers[$header] = $value;

        return $this;
    }

    /**
     * Add a file attachment. Warning, this will force POST requests.
     *
     * @param array $config
     * @return Client
     */
    public function addfile(String $name, String $filepath): Client
    {
        array_push(
            $this->files,
            [
                'name' => $name,
                'filepath' => $filepath,
            ]
        );

        return $this;
    }

    /**
     * Executes http request
     * @param  \Engeni\ApiClient\Query $query
     * @return Guzzle\Response
     */
    public function execute($verb, $resource = '', $params = []): GuzzleResponse
    {
        $http = $this->getHttpClient();
        try {
            $result = $http->request($verb, $resource, $params);
            if ($result->getStatusCode() >= 400) {
                throw new ApiResponseException($result->getBody()->getContents(), $result->getStatusCode());
            }
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            throw new \Exception($response->getBody()->getContents(), $e->getCode());
        }
        return $result;
    }

    /**
     * \Engeni\ApiClient\Query dependant method get
     * @param  \Engeni\ApiClient\Query $query
     * @return Guzzle\Response
     */
    public function get(\Engeni\ApiClient\Query $query): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = ['query' => $query->getPreparedQuery(), 'debug' => $this->config['debug']];

        return $this->execute('GET', $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get
     * @param  \Engeni\ApiClient\Query $query
     * @return Guzzle\Response
     */
    public function put(\Engeni\ApiClient\Query $query, $form_params = []): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = $this->getCompiledFormParams($form_params);

        // Unforutnately, Laravel does not suport multipart form data with PUT method.
        // Se we have to add _method PUT to resolve it.
        if ($this->isMultipart) {
            $method = 'POST';
            $params['multipart'][] = [
                'name' => '_method',
                'contents' => 'PUT',
            ];
        } else {
            $method = 'PUT';
        }

        return $this->execute($method, $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get
     * @param  \Engeni\ApiClient\Query $query
     * @return Guzzle\Response
     */
    public function post(\Engeni\ApiClient\Query $query, $form_params = []): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = $this->getCompiledFormParams($form_params);

        return $this->execute('POST', $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get
     * @param  \Engeni\ApiClient\Query $query
     * @return Guzzle\Response
     */
    public function delete(\Engeni\ApiClient\Query $query): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = ['query' => $query->getPreparedQuery(), 'debug' => $this->config['debug']];

        return $this->execute('DELETE', $resource, $params);
    }

    /**
     * Compiles the form_params por the post-put method
     *
     * Note:
     * multipart cannot be used with the form_params option.
     * You will need to use one or the other. Use form_params
     * for application/x-www-form-urlencoded requests,
     * and multipart for multipart/form-data requests.
     * This option cannot be used with body, form_params, or json.
     *
     * See https://docs.guzzlephp.org/en/stable/request-options.html#multipart for more info.
     *
     * @return Array
     */
    protected function getCompiledFormParams(array $form_params = []): array
    {
        // If it has files, then it's multipart
        if (!empty($this->files)) {
            $this->isMultipart = true;
            $params['multipart'] = [];

            foreach ($this->files as $file) {
                array_push(
                    $params['multipart'],
                    [
                        'name' => $file['name'],
                        'contents' => fopen($file['filepath'], 'r'),
                        'filename' => basename($file['filepath']),
                    ]
                );
            }
        }
        // otherwise it's handled has form_params
        else {
            $this->isMultipart = false;
            $params['form_params'] = $form_params;
        }

        // debug_param
        $params['debug'] = $this->config['debug'];

        return $params;
    }

    /**
     * Set the Http Client object
     * @param GuzzleHttp\ClientInterface $http
     * @return Client
     */
    public function setHttpClient(ClientInterface $http): Client
    {
        $this->http = $http;

        return $this;
    }

    /**
     * @return GuzzleHttp\ClientInterface implementation
     */
    public function getHttpClient()
    {
        if (null === $this->http) {
            $this->http = $this->createDefaultHttpClient();
        }

        return $this->http;
    }

    /**
     * @return GuzzleHttp\ClientInterface implementation
     */
    protected function createDefaultHttpClient()
    {
        // ClientInterface::VERSION is defined only for versions < 7.0
        if (defined('\GuzzleHttp\ClientInterface::VERSION')) {
            // guzzle 6
            $version = \GuzzleHttp\ClientInterface::VERSION ?? null;
            if ($version < 6) {
                throw new Exception("Guzzle must be version 6 or newer");
            }
        }
        // At this point Guzzle version is >= 6

        $options = [
            'exceptions' => false,
            'timeout' => $this->config['timeout'],
            'headers' => array_merge(array_merge([
                'User-Agent' => $this->config['application_name'] . " " . self::USER_AGENT_SUFFIX . $this->getLibraryVersion(),
                'Accept' => 'application/json',
                'Authorization' => $this->config['auth_token'],
            ], $this->headers), $this->config['headers']),
            'base_uri' => $this->config['base_uri'],
            'base_path' => $this->config['base_path'],
            'verify' => $this->config['verify'],
        ];

        if(strpos($options['headers']['Authorization'], 'Bearer') !== 0) {
            $options['headers']['Authorization'] = 'Bearer '.$options['headers']['Authorization'];
        }

        return new \GuzzleHttp\Client($options);
    }

    /** Get a string containing the version of the library.
     *
     * @return string
     */
    public function getLibraryVersion()
    {
        return self::VERSION;
    }
}
