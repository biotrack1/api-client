<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2021 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Paginator implements \Countable, \Iterator
{
    private $items = [];
    public $current_page = 0;
    public $first_page_url = "";
    public $from = 0;
    public $last_page = 0;
    public $last_page_url = "";
    public $next_page_url = "";
    public $path = "";
    public $per_page = 0;
    public $prev_page_url = "";
    public $to = 0;
    public $total = 0;

    // for the Iterator implementation
    private $position = 0;

    /**
     * Create a new Resource recource instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct($options = [])
    {
        // Setting up all variables
        foreach ($options as $key => $value) {
            if (isset($this->$key)) {
                $this->{$key} = $value;
            }
        }
        $this->items = isset($options['data']) && is_array($options['data']) ? $options['data'] : $this->items;
    }

    /**
     * Resolve the current page or return the default value.
     *
     * @param  string  $pageName
     * @param  int  $default
     * @return int
     */
    public static function resolveCurrentPage($pageName = 'page', $default = 1)
    {
        $page = $_GET[$pageName] ?? null;

        if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
            return (int) $page;
        }

        return 1;
    }

    /**
     * Extends funcionality for Countable
     *
     * @return integer
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Extends funcionalities for Iterator
     *
     * @return integer
     */
    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->items[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->items[$this->position]);
    }
}
