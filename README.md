Forian API Client is a client for take profit of the Forian Resources.
- Navigate through Forian resources
- Fetch Forian Stats
- Much more

Please note that you need an Forian API Key to access all Forian API resources. Please, get in contact with Forian at: info@forian.com.

### Installation
Install composer first. Then:

```sh
$ composer require forian/api-client
```
Enjoy.
The Forian Team.